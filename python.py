# Caleb Freeman
# Ashley Simmons
# CS4500-002
# 8/27/19


import random

rSeed = random.randint(1, 100)
random.seed(rSeed)  # seeding with rSeed
atBats = random.randint(1, 1000)
batAverage = (random.randint(1, 1000)) * .001

print(rSeed)
print(batAverage)
print(atBats)

with open('InClass2infile.txt', 'w') as f:
    f.write(str(atBats) + "\n")
    f.write(str(rSeed) + "\n")
    f.write(str(batAverage) + "\n")

with open('InClass2outfile.txt', 'w') as f:
    f.write(str(rSeed) + "\n")
    f.write(str(batAverage) + "\n")
    f.write(str(atBats) + "\n")

with open('InClass2infile.txt', 'r') as g:
    battingArr = []
    lines = g.readlines()
    try:
        for line in lines:
            battingArr.append(str(line))
    except:
        print("Out of bounds")
        exit(1)                     # exception error if reads input out of bounds

# assigning values from inputfile to variables
atBats = int(battingArr[0])
rSeed = float(battingArr[1])
batAverage = float(battingArr[2])

# atbatsCounter for while loop and theBatter list for maintaining batters performance
atBatsCounter = atBats
theBatter = []

# while loop for running simulation
# If the number you throw is less than or equal to the BatAverage, then the batter gets a hit. If the number you throw
# is greater than BatAverage, the batter does not get a hit. Keep doing this AtBats times.
random.seed(rSeed)  # seeding with rSeed
while atBatsCounter != 0:
    throw = (random.randint(1, 1000)) * .001
    if throw <= batAverage:
        theBatter.append(1)
        atBatsCounter -= 1
    elif throw > batAverage:
        theBatter.append(0)
        atBatsCounter -= 1
    else:
        print("wait a min")

# sum of theBatter list equal to total hits
totalHits = sum(theBatter)
# simulation batters average
battersAverage = totalHits / atBats
# calculating the number of "hits in a row"
consecutiveHits = 1
tempHits = 0

for i in theBatter:
    if i == 1:
        tempHits += 1
    else:
        if tempHits > consecutiveHits:
            consecutiveHits = tempHits
        tempHits = 0

with open('InClass2outfile.txt', 'a') as f:
    f.write("\n")
    f.write(str(totalHits) + "\n")
    f.write(str(battersAverage) + "\n")
    f.write(str(consecutiveHits))
